#Beleg - Game Programming

## Anleitung

Um das Spiel korrekt ausführen zu können müssen die Dateien über einen Web-Server ausgeliefert werden.
Es kann durch Aufruf der Datei ``index.html`` gestartet werden.


Eine bereits vorbereitete gehostete Version kann über die URL http://xmyno.me/gdflap/index.html gespielt werden.


Beispielhafte Konfiguration für nginx:

```
server {
    server_name localhost;
    listen      80 default_server;
    root        <directory of repo>;

    location / {
        index index.html index.php;
    }
}
```
