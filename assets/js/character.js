// global namespace of the project
this.gdflap = this.gdflap || {};

(function () {

    // constructor for Character object
    function Character () {
        this._width = gdflap.Constants.PLAYER.WIDTH;
        this._height = gdflap.Constants.PLAYER.HEIGHT;

        this.velocity = 0;
        this.gravity = gdflap.Constants.PLAYER.GRAVITY;

        // create the player easeljs object with character image
        this.view = new createjs.Bitmap("assets/images/char.png");

        // create the wing of the bird (to move it seperately from body)
        this.wing = new createjs.Shape();
        this.wing.graphics.beginFill("#fff").drawRect(0, 0, 9, 4);
        this.wing.offsetY = 14;
        this.wing.animationDirection = -1; // start animating upwards

        // update stage when image has finished loading
        // prevents image not showing on first stage render
        this.view.image.onload = function () {
            gdflap.game.level.stage.update();
        }

        this._spawn();
    }

    // Character object declaration
    Character.prototype = {
        constructor: Character,

        // set the character to its initial position
        _spawn: function () {
            this._x = gdflap.Constants.PLAYER.START_POS_X;
            this._y = gdflap.Constants.PLAYER.START_POS_Y;
            this.view.x = this._x;
            this.view.y = this._y;
            this.wing.x = this._x + 3;
            this.wing.y = this._y + this.wing.offsetY;
        },

        // check collision with obstacles (box collision)
        _checkBoxCollision: function (obstacle) {
            if (this._x < obstacle.x + obstacle.width &&
                this._x + this._width > obstacle.x &&
                this._y < obstacle.y + obstacle.height &&
                this._height + this._y > obstacle.y) {

                return true;
            }
            return false;
        },

        // makes the character fly upwards
        boost: function() {
            // play click sound
            gdflap.sounds.click.currentTime = 0;
            gdflap.sounds.click.play();

            // set velocity to fixed positive value
            this.velocity = gdflap.Constants.PLAYER.VELOCITY_BOOST;
        },

        // checks all possible collision
        collides: function () {
            // check collision with floor
            if (this._y + this._height > gdflap.game.level.floorY) return true;
            // check collision with ceiling
            if (this._y < gdflap.game.level.ceilingY) return true;
            // check collision with active obstacle
            this.currentObstacleSet = gdflap.game.level.obstacleSets[gdflap.game.level.nextObstacleIndex];
            if (this._checkBoxCollision(this.currentObstacleSet.upperObstacle)) return true;
            if (this._checkBoxCollision(this.currentObstacleSet.lowerObstacle)) return true;

            // no collision, yay!
            return false;
        },

        // handles tick event
        tick: function (deltaTime) {
            // decrease velocity of the character
            if (this.velocity > gdflap.Constants.PLAYER.MIN_VELOCITY) {
                this.velocity -= gdflap.Constants.PLAYER.GRAVITY * deltaTime;
            }
            // move the game object of the character
            this._y += -1 * (this.velocity * deltaTime);

            // animate the wing
            if (this.wing.offsetY >= 14) this.wing.animationDirection = -1;
            if (this.wing.offsetY <= 8) this.wing.animationDirection = 1;
            this.wing.offsetY += this.wing.animationDirection * 0.05 * deltaTime;

            // update the view objects of character and wing
            this.view.y = this._y;
            this.wing.y = this._y + this.wing.offsetY;
        },

        // respawn the character
        reset: function () {
            this._spawn();
            this.velocity = 0;
        },

    };

    // handover to global namespace
    this.gdflap.Character = Character;

}());
