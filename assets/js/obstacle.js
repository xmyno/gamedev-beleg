// global namespace of the project
this.gdflap = this.gdflap || {};

(function () {

    // constructor for Obstacle objects
    function Obstacle (x, obstacleHeight) {
        this.startPosX = x;
        this.height = obstacleHeight;
        this.width = 40;

        // Create a new EaselJS reactangular shape
        this.view = new createjs.Shape();
        this.view.graphics.beginFill(gdflap.Constants.COLORS.main).drawRect(0, 0, this.width, this.height);

        this._spawn();
    };

    // Obstacle object declaration
    Obstacle.prototype = {
        constructor: Obstacle,

        // resets to the original position
        _spawn: function () {
            this.setXPosition(this.startPosX);
        },

        // sets x position of game and view object
        setXPosition: function (x) {
            this.x = x;
            this.view.x = this.x;
        },

        // sets y position of game and view object
        setYPosition: function (y) {
            this.y = y;
            this.view.y = this.y;
        },
    };

    // constructor for ObstacleSet objects
    function ObstacleSet (x, levelHeight) {
        this.spawnPosX = x;
        this.x = this.spawnPosX;
        this.obstacleHeight = (levelHeight / 2) + 100;
        this.gapY = this.selectRandomGapY();
        this.gapHeight = gdflap.Constants.GAP_HEIGHT;

        this.upperObstacle = new Obstacle(this.spawnPosX, this.obstacleHeight);
        this.lowerObstacle = new Obstacle(this.spawnPosX, this.obstacleHeight);

        this.setGapPos(this.gapY);
    };

    // ObstacleSet objects declaration
    // ObstacleSet consists of two Obstacle objects, one upper and one lower
    ObstacleSet.prototype = {
        constructor: ObstacleSet,

        setGapPos: function (gapY) {
            // pos is always top-left corner, gap pos is top-left corner of gap as well
            // y coord gets bigger from top to bottom
            this.upperObstacle.setYPosition(gapY - this.upperObstacle.height);
            this.lowerObstacle.setYPosition(gapY + this.gapHeight);
        },

        selectRandomGapY: function () {
            // get a random y pos for the gap
            return gdflap.Constants.GAP_POSITIONS[ Math.floor(Math.random() * gdflap.Constants.GAP_POSITIONS.length) ];
        },

        changeActiveObstacle: function () {
            // if index is last array elem restart at 0
            if (gdflap.game.level.nextObstacleIndex == gdflap.game.level.obstacleSets.length - 1) {
                gdflap.game.level.nextObstacleIndex = 0;
                return;
            }
            // else, just increment
            gdflap.game.level.nextObstacleIndex++;
        },

        // implementation of a tick method, called by the game loop
        tick: function (deltaTime) {
            // increment the active obstacle index for collision check
            // if obstacle set has passed the player character
            if (!this.indexMoved && this.x < 160) {
                // set indexMoved so incrementing active obstacle only once
                // gets reset when the obstacle set is respawned at the end of the level
                this.indexMoved = true;
                gdflap.scores.current.innerHTML = ++gdflap.game.score;
                this.changeActiveObstacle();
            }

            // obstacle is off screen, "respawn" it at the end of the level
            // and calculate a new random gap position
            if (this.x < -50) {
                this.x = gdflap.Constants.OBSTACLE_RESPAWN_POS_X;
                this.gapY = this.selectRandomGapY();
                this.setGapPos(this.gapY);
                this.indexMoved = false;
            } else {
                // move the obstacles towards the player
                this.x = this.x - (gdflap.Constants.LEVEL_SPEED * deltaTime);
            }

            // actually move the obstacle objects of this set
            this.upperObstacle.setXPosition(this.x);
            this.lowerObstacle.setXPosition(this.x);
        },

        // reset the obstacle set to its initial position when the game is restarted
        reset: function () {
            // set obstacles to initial pos
            this.x = this.spawnPosX;
            this.upperObstacle._spawn();
            this.lowerObstacle._spawn();

            // choose a new gap
            this.gapY = this.selectRandomGapY();
            this.setGapPos(this.gapY);

            // reset the next obstacle to the first
            gdflap.game.level.nextObstacleIndex = 0;
            this.indexMoved = false;
        },
    };

    // handover to global namespace
    this.gdflap.ObstacleSet = ObstacleSet;

}());
