// global namespace of the project
this.gdflap = this.gdflap || {};

(function () {

    // constructor for Level objects
    function Level () {
        // get width and height of the HTML <canvas> element
        var canvas = document.getElementById(gdflap.Constants.CANVAS_ID);
        this.dimensions = {
            width: canvas.width,
            height: canvas.height,
        };

        // create an easeljs stage
        this.stage = new createjs.Stage(gdflap.Constants.CANVAS_ID);
    }

    // Level object declaration
    Level.prototype = {
        constructor: Level,

        // initialize the level
        loadLevel: function () {
            // create the room
            var roomBg = new createjs.Shape();
            roomBg.graphics.beginFill(gdflap.Constants.COLORS.bg).drawRect(0, 0, this.dimensions.width, this.dimensions.height);
            roomBg.x = 0;
            roomBg.y = 0;

            var roomFloor = new createjs.Shape();
            roomFloor.graphics.beginFill(gdflap.Constants.COLORS.main).drawRect(0, 0, 800, 50);
            roomFloor.x = 0;
            roomFloor.y = this.dimensions.height - 50;
            this.floorY = roomFloor.y;

            var roomCeiling = new createjs.Shape();
            roomCeiling.graphics.beginFill(gdflap.Constants.COLORS.main).drawRect(0, 0, 800, 50);
            roomCeiling.x = 0;
            roomCeiling.y = 0;
            this.ceilingY = roomCeiling.y + 50;

            // create obstacle sets and save them in an array for later use
            this.obstacleSets = [];

            this.obstacleSets.push(new gdflap.ObstacleSet(400, this.dimensions.height));
            this.obstacleSets.push(new gdflap.ObstacleSet(550, this.dimensions.height));
            this.obstacleSets.push(new gdflap.ObstacleSet(700, this.dimensions.height));
            this.obstacleSets.push(new gdflap.ObstacleSet(850, this.dimensions.height));
            this.obstacleSets.push(new gdflap.ObstacleSet(1000, this.dimensions.height));
            this.obstacleSets.push(new gdflap.ObstacleSet(1150, this.dimensions.height));

            // set active obstacle to first
            this.nextObstacleIndex = 0;

            // create instruction text (text below character at beginning of game)
            this.instructionText = new createjs.Text("CLICK OR SPACE\nTO FLAP", "36px Agency", "#00bfff");
            this.instructionText.x = 215;
            this.instructionText.y = 185;
            this.instructionText.lineHeight = 36;
            this.instructionText.textAlign = 'center';


            // add everything to the easeljs stage
            this.stage.addChild(roomBg);
            this.stage.addChild(roomFloor);
            this.stage.addChild(roomCeiling);
            this.stage.addChild(this.instructionText);

            this.obstacleSets.forEach((function (obstacleSet) {
                this.stage.addChild(obstacleSet.upperObstacle.view);
                this.stage.addChild(obstacleSet.lowerObstacle.view);
            }).bind(this));
        },
    };

    // handover to global namespace
    this.gdflap.Level = Level;

}());
