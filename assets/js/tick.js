// global namespace of the project
this.gdflap = this.gdflap || {};

(function() {

    // constructor for Tick objects
    function Tick() {
        this._FPS = 60;
        this._paused = true;

        // the current elapsed time of the js execution
        this._startTime = window.performance.now();
        this._updateInterval();
    }

    // Tick object declaration
    // Tick is the object for the global game loop,
    // handling starting, pausing and firing game loop events
    Tick.prototype = {
        constructor: Tick,

        // setter for fps value
        setFPS: function (fps) {
            this._FPS = fps;
            this._updateInterval();
        },

        // toggle paused state
        // if not using RAF cancel the interval method call to
        togglePause: function () {
            this._paused = !this._paused;
            if (!this._paused) {
                this.start();
            } else {
                // if using setInterval cancel the interval using the id
                // otherwise using RAF the loop cancels itself depending on _paused
                if (this._intervalId) {
                    window.clearInterval(this._intervalId)
                    delete this._intervalId
                }
            }
        },

        // returns if the game is paused
        isPaused: function() {
            return this._paused;
        },

        // starts the main game loop
        start: function () {
            this._paused = false;
            // initialize tick time for the first tick
            this._tickTime = window.performance.now();

            // if RAF is available use it, otherwise fall back to using setInterval for the loop
            if (window.requestAnimationFrame) {
                this._rafLoop();
            } else {
                // setInterval calls the function given to it in a specified interval (e.g. every 16.6.. ms for 60 fps)
                // returns an id, used to cancel the interval
                this._intervalId = window.setInterval(this._tick.bind(this), this._interval);
            }
        },

        // calculates interval in milliseconds
        _updateInterval: function () {
            this._interval = 1000 / this._FPS;
        },

        // main game "loop" using the requestAnimationFrame method available in modern browsers,
        // basically limiting the amount of ticks to the monitors refresh rate
        _rafLoop: function () {
            // breaks game loop
            if (this._paused) return;
            // call the tick method, firing a tick event
            this._tick();
            // give the loop function as a callback to the RAF method
            // (this method prevents blocking the js function call stack if it would be using an endless loop,
            // or exceed max call stack size if it would use recursive function calls)
            window.requestAnimationFrame(this._rafLoop.bind(this));
        },

        // calculates time since last tick and fires a tick event notifying listeners of the tick
        // deltaTime is used for updating physics independent of the render fps
        _tick: function () {
            // calculate time since last tick
            this._prevTickTime = this._tickTime;
            this._tickTime = window.performance.now();
            this._deltaTime = this._tickTime - this._prevTickTime;

            // create a js event that can be listened to (called 'tick')
            // tells listeners the expired time since last tick
            var tickEvent = new CustomEvent(
                'tick',
                {
                    detail: {
                      deltaTime: this._deltaTime
                    }
                }
            );

            // dispatch the event, notifying every listener of the event
            document.dispatchEvent(tickEvent);
        },
    };

    // handover to the global namespace
    this.gdflap.Tick = Tick;

}());
