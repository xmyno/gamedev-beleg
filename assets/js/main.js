// global namespace of the project
this.gdflap = this.gdflap || {};

// constructor for Game object
function Game() {

    this.tick = new gdflap.Tick();
    this.character = new gdflap.Character();
    this.level = new gdflap.Level();

    this.isGameOver = false;
    this.score = 0;

    // load level and add character
    this.level.loadLevel();
    this.level.stage.addChild(this.character.view);
    this.level.stage.addChild(this.character.wing)

    // enable better mobile device input
    createjs.Touch.enable(this.level.stage);

    // update the easeljs stage
    this.level.stage.update();


    // listen to game loops tick events
    document.addEventListener('tick', this.handleTick.bind(this));
    // listen to mousedown events
    this.level.stage.on('mousedown', this.click.bind(this));

}

// Game object declaration
Game.prototype = {

    constructor: Game,

    // handles user input (mouse left down event, space key down event)
    // if not gameover; starts game when paused, controls character
    click: function () {
        if (!this.isGameOver) {
            if (this.tick.isPaused()) {
                this.tick.start();
                this.level.stage.removeChild(this.level.instructionText);
            }

            this.character.boost();
        }
    },

    // reset the game to initial state
    restartGame: function () {
        this.isGameOver = false;
        gdflap.scores.current.innerHTML = this.score = 0;

        this.character.reset();
        this.level.obstacleSets.forEach((function (obstacleSet) {
            obstacleSet.reset();
        }).bind(this));

        gdflap.gameOverOverlay.style.display = 'none';
        this.level.stage.addChild(this.level.instructionText);
        this.level.stage.update();
    },

    // implementation of the tick event listener callback
    handleTick: function (event) {
        // elapsed time since last tick, stored in the event content
        var deltaTime = event.detail.deltaTime;

        // call tick methods of objects that need to be updated
        this.character.tick(deltaTime);
        this.level.obstacleSets.forEach(function (obstacleSet) {
            obstacleSet.tick(deltaTime);
        });

        // check character collision
        if (this.character.collides()) {
            this._endGame();
        }

        // update the easeljs stage after every tick
        this.level.stage.update();
    },

    _endGame: function () {
        gdflap.sounds.fail.play();
        this.isGameOver = true;

        // update record
        if (!localStorage.record || this.score > localStorage.record) {
            localStorage.setItem('record', this.score);
            gdflap.scores.record.innerHTML = localStorage.record;
        }

        // show game over overlay
        gdflap.gameOverOverlay.style.display = 'block';
        // pause the game loop
        this.tick.togglePause();
    },

};

window.addEventListener('load', function () {
    // create a new game object
    this.gdflap.game = new Game();

    // initialize sounds
    this.gdflap.sounds = {};
    this.gdflap.sounds.fail = document.getElementById('gdflap-sound-fail');
    this.gdflap.sounds.fail.volume = 0.5;
    this.gdflap.sounds.click = document.getElementById('gdflap-sound-click');
    this.gdflap.sounds.click.volume = 0.5;

    // initialize html score fields
    this.gdflap.scores = {};
    this.gdflap.scores.record = document.getElementById('gdflap-score-record');
    this.gdflap.scores.current = document.getElementById('gdflap-score-current');
    this.gdflap.scores.record.innerHTML = localStorage.record || 0;

    // save game overlay option for later use
    this.gdflap.gameOverOverlay = document.getElementById('gdflap-game-overlay');

    // restart the game when hitting the retry button in game over screen
    this.gdflap.retryBtn = document.getElementById('gdflap-retry-btn');
    this.gdflap.retryBtn.addEventListener('click',
        this.gdflap.game.restartGame.bind(this.gdflap.game)
    );

    // bind space key to functionality of mouse left click
    this.addEventListener('keydown', function (event) {
        if (event.keyCode === 0 || event.keyCode === 32) {
            // prevent scrolling down the page when hitting space
            event.preventDefault();

            if (!this.gdflap.game.isGameOver) {
                this.gdflap.game.click();
            } else {
                this.gdflap.game.restartGame();
            }
        }
    });

}, false);
