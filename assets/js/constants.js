// global namespace of the project
this.gdflap = this.gdflap || {};

(function () {

    // constants declarations
    var Constants = {

        CANVAS_ID: 'gdflap-canvas',
        COLORS: {
            main: '#00bfff', // baby-blue
            bg: '#ffffef', // yellow-beige
        },
        LEVEL_SPEED: 0.1,
        // LEVEL_SPEED: 0.2, // HARDMODE >:)

        PLAYER: {
            START_POS_X: 200,
            START_POS_Y: 150,
            WIDTH: 23,
            HEIGHT: 23,

            GRAVITY: 0.00125,
            // GRAVITY: 0.002, // HARDMODE >:)
            MIN_VELOCITY: -0.75,
            VELOCITY_BOOST: 0.3,
            // VELOCITY_BOOST: 0.35, // HARDMODE >:)
        },

        OBSTACLE_RESPAWN_POS_X: 850,
        GAP_POSITIONS: [
            100,
            125,
            150,
            175,
            200,
            225,
        ],
        GAP_HEIGHT: 80,

    }

    // handover to global namespace
    this.gdflap.Constants = Constants;

}());
